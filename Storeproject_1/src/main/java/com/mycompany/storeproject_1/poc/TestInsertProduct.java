package com.mycompany.storeproject_1.poc;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class TestInsertProduct {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();
        try {
            String sql = "INSERT INTO product (name,price) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Product product = new Product(-1,"Oh Leing",20);
            stmt.setString(1,product.getName());
            stmt.setDouble(2,product.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affec row " + row + " id: " + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestInsertProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }

}
