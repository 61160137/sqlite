
import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc.sqlite:" + dbPath);
            System.out.println("Database connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connection");
        }
        try {
            String sql = "SELECT id,name,price FROM product";
            Statement stmt = (Statement) conn.createStatement();
            ResultSet result =  stmt.excuteQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getNString("name");
                double price = result.getDouble("price");
                System.out.println(id+" "+name+" "+price);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error: Database clase connection");
        }
    }
}
